#!/usr/bin/env python3

from ebooklib import epub

path_v = "volume-001"
imgs = ['01.jpg', '02.jpg', '03.jpg', '05.jpg']

if __name__ == '__main__':
    book = epub.EpubBook()

    # add metadata
    book.set_identifier('Index')
    book.set_title('Index volume 001')
    book.set_language('fr')

    book.add_author('Kamachi Kazuma')

    # add cover image
    book.set_cover("cover.jpg", open(f'{path_v}/01.jpg', 'rb').read())

    pages = []
    nb = 1
    for img in imgs:
        print("page !")
        add_img = epub.EpubItem(file_name=f'{img}', content=open(f'{path_v}/{img}', 'rb').read(), media_type='image/jpg')
        page = epub.EpubHtml(title=img, file_name=f'{nb}.xhtml', lang='fr')
        page.content=f'<img src="{img}" alt="{img}"/>'

        book.add_item(add_img)
        book.add_item(page)
        nb += 1
        pages.append(page)

    # add navigation files
    book.add_item(epub.EpubNcx())
    book.add_item(epub.EpubNav())

    l = ['cover'] + pages
    print(l)
    book.spine = l

    # create epub file
    epub.write_epub('test.epub', book)
