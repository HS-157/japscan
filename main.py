#!/usr/bin/env python3

import japscan
from pprint import pprint

# a = japscan.list_all_manga()
# pprint(a)

# b = japscan.list_chapitre("rengoku-no-karma")
# print("=========")
# pprint(b)
# print("=========")
# 
# print("=========")
# c = japscan.list_chapitre("ranma-12")
# pprint(c)
# print("=========")
# 
# d = japscan.list_page('/lecture-en-ligne/ranma-12/volume-12/')
# pprint(d)
# 
# e = japscan.get_img('/lecture-en-ligne/ranma-12/volume-12/154.html')
# print(e)
# 
# japscan.download_img("https://cdn.japscan.cc/lel/Ranma-1-2/Scan-Ranma-1-2-Tome-12-VF/154.jpg")

import pathlib

mangas = ["a-certain-magical-index"]

for manga in mangas:
    chaps = japscan.list_chapitre(manga)
    pages = {}
    for chap in chaps:
        status = chaps[chap]["status"]
        nb = chaps[chap]["nb"]
        uri = chaps[chap]["uri"]

        print(f"Get list page - {manga} - {status} {chap}", end="", flush=True)

        path_dir = f"./{manga}/{status}-{nb:03}"
        pathlib.Path(path_dir).mkdir(parents=True, exist_ok=True) 

        pages[path_dir] = japscan.list_page(uri)
        print()
    
    for path_dir, pages_img in pages.items():
        for img in pages_img:
            print(f"Download : {img['url']}") 
            japscan.download_img(img, path=path_dir)
