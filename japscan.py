#!/usr/bin/env python3

from bs4 import BeautifulSoup
import requests
from pprint import pprint
import re


def get_url(list_manga=None, manga=None, chapitre=None, page=None):
    base = "https://www.japscan.cc"

    if list_manga:
        url = f"{base}/mangas/"
    elif manga:
        url = f"{base}/mangas/{manga}/"
    elif chapitre:
        url = f"{base}{chapitre}"
    elif page:
        url = f"{base}{page}"
    
    return url

def get_html(url):
    requete = requests.get(url)
    page = requete.content
    return BeautifulSoup(page, "html.parser")


def list_all_manga():
    manga = {}
    url = get_url(list_manga=True)

    html = get_html(url)

    list_manga = html.find(id="liste_mangas")

    for p in list_manga.find_all("div", {"class": "row"}):
        for i in p.div:
            name = i.get_text()
            uri = i.get("href")
            name_low = uri.replace("/mangas/", "").replace("/", "")
            manga[name_low] = {
                "uri": uri,
                "name": name
            }

    return manga

def list_chapitre(manga):
    list_chapitre = {}

    url = get_url(manga=manga)

    html = get_html(url)
    list_chapitre_html = html.find(id="liste_chapitres")
    list_a = list_chapitre_html.find_all("a")
    list_a.reverse()

    for a in list_a:
        uri = a.get("href")
        uri_split = uri.split("/")
        uri_split.reverse()

        for split in uri_split:
            if split.isdigit() or "volume-" in split:
                if "volume-" in split:
                    nb = split.replace("volume-", "")
                    nb = int(nb)
                    status = "volume"
                else:
                    nb = int(split)
                    status = "chapitre"
                break

        list_chapitre[nb] = {
            "status": status,
            "nb": nb,
            "uri": uri
        }

    return list_chapitre



def list_page(chapitre):

    pages = []

    url = get_url(chapitre=chapitre)
    
    html = get_html(url)

    list_pages = html.find(id="pages")

    for page in list_pages.find_all('option'):
        img = get_img(page.get("value"))
        pages.append(img)

    return pages



def get_img(page):

    url = get_url(page=page)
    
    html = get_html(url)

    img = html.find("img").get("src")
    img_split = img.split("/")
    nb, ext = img_split[-1].split(".")

    print(".", end="", flush=True)

    return {
        "url": img,
        "nb": nb,
        "ext": ext
    }


def download_img(img, path="."):
    url = img["url"]
    nb = img["nb"]
    ext = img["ext"]
    img_data = requests.get(url).content

    if not nb.isdigit:
        print("===============")
        print(nb)
        print("===============")
        nb = f"{nb:04}"

    with open(f"{path}/{nb}.{ext}", 'wb') as handler:
        handler.write(img_data)
